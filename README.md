<!-- omit in toc -->
:warning: 由於 GitLab 針對 2021-05-17 後註冊的用戶修改了 GitLab CI/CD 的使用規則，用戶必須提供信用卡資訊或使用已購買 CI/CD 分鐘數的 `namespaces`，否則無法透過官方管理的 Runner 來執行 CI/CD。若在不滿足上述條件的情況下，仍要使用 GitLab CI/CD，則需要自行建立 Runner。
> [GitLab 官方說明](https://about.gitlab.com/blog/2021/05/17/prevent-crypto-mining-abuse/)

本專案將說明如何自建 Runner，並透過該 Runner 執行我們的 CI/CD 任務。

- [架設 Gitlab Runner](#架設-gitlab-runner)
  - [建立自管理的 Runner (Docker)](#建立自管理的-runner-docker)
  - [加強變更保護機制](#加強變更保護機制)
  - [建立 CI/CD Pipeline](#建立-cicd-pipeline)
- [安全問題與修正](#安全問題與修正)

# 架設 Gitlab Runner
## 建立自管理的 Runner (Docker)
1. 從 GitLab Group/Project 選單 `Settings` > `CI/CD` > `Runner` > `Specific runners` 取得註冊 token
2. 以 Docker 方式安裝 Runner
    ```sh
    # 若無法使用 ${PWD}，請改為當前資料夾的絕對路徑
    $ docker run -d --name gitlab-runner --restart always \
      -v ${PWD}/gitlab-runner:/etc/gitlab-runner \
      -v /var/run/docker.sock:/var/run/docker.sock \
      gitlab/gitlab-runner:alpine3.12-v14.10.1
    ```
    執行後，會在當前的資料夾下生成 `gitlab-runner` 資料夾，並運行一個 `gitlab-runner` 的容器：
    ```sh
    $ ls
    gitlab-runner

    $ docker ps
    CONTAINER ID   IMAGE                                      COMMAND                  CREATED          STATUS          PORTS     NAMES
    6d37487bbe43   gitlab/gitlab-runner:alpine3.12-v14.10.1   "/usr/bin/dumb-init …"   41 seconds ago   Up 40 seconds             gitlab-runner
    ```
3. 註冊 Runner
    ```sh
    # 若無法使用 ${PWD}，請改為當前資料夾的絕對路徑
    $ docker run --rm -it \
      -v ${PWD}/gitlab-runner:/etc/gitlab-runner \
      gitlab/gitlab-runner:alpine3.12-v14.10.1 register
    ```
    並依照以下資訊輸入：
    * **GitLab URL:** https://gitlab.com/
    * **registration token:** 填入步驟 1 取得的註冊 token
    * **description:** (optional)
    * **tags for the runner:** demo
    * **optional maintenance note:**  (optional)
    * **executor:** docker (使用 dind 的方式運行)
    * **default Docker image:** docker:20.10.20

    執行結果：
    ```sh
    Runtime platform                                    arch=amd64 os=linux pid=7 revision=f761588f version=14.10.1
    Running in system-mode.

    # 輸入 GitLab 位址：
    Enter the GitLab instance URL (for example, https://gitlab.com/):
    https://gitlab.com/
    Enter the registration token:
    <gitlab-runner-token>
    # 輸入 Runner 描述（選填）：
    Enter a description for the runner:
    [2214bd0669bb]:
    # 輸入 Runner 標籤：
    Enter tags for the runner (comma-separated):
    demo
     # 輸入其餘描述（選填）：
    Enter optional maintenance note for the runner:

    # 輸出註冊狀態
    dRegistering runner... succeeded                     runner=GR1348941P37yRRz9
    # 輸入任務運行環境
    Enter an executor: docker, docker-ssh, parallels, docker+machine, docker-ssh+machine, kubernetes, custom, ssh, virtualbox, shell:
    docker  # 選擇 docker
    # 輸入任務運行環境所使用的映像檔
    Enter the default Docker image (for example, ruby:2.7):
    docker:20.10.20
    # 輸出註冊狀態
    Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
    ```
4. 上述步驟執行後，會在 `gitlab-runner` 資料夾下生成 `config.toml`，此即為 Runner 的設定檔
    ```yml
    concurrent = 1
    check_interval = 0

    [session_server]
    session_timeout = 1800

    [[runners]]
    name = "2214bd0669bb"
    url = "https://gitlab.com/"
    token = "5tTpmsuXyUbAkmgF77Ss"
    executor = "docker"
    [runners.custom_build_dir]
    [runners.cache]
      [runners.cache.s3]
      [runners.cache.gcs]
      [runners.cache.azure]
    [runners.docker]
      tls_verify = false
      image = "docker:20.10.20"
      privileged = false
      disable_entrypoint_overwrite = false
      oom_kill_disable = false
      disable_cache = false
      volumes = ["/cache"]
      shm_size = 0
   ```
    > :exclamation: 由於有些設定 GitLab 並無在互動時輸入，因此雖然此 Runner 狀態顯示註冊成功，但 config.toml 中的設定仍需要進行修改！
5. 編輯掛載到本機端的 Runner 設定檔 `config.toml`
    ```sh
    # 若無法使用 ${PWD}，請改為當前資料夾的絕對路徑
    $ vim ${PWD}/gitlab-runner/config.toml
    ```
    採用 Docker executor 需修改 `runners.docker` 內的 `privileged` 與 `volumes` 欄位，如下：
    > [官方文件](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker)
    ```yml
    [runners.docker]
      ...
      privileged = true
      ...
      volumes = ["/cache","/certs/client"]
    ```
6. 從 `Settings` > `CI/CD` > `Runner` > `Specific runners` 確認剛註冊 Runner 為可用狀態（綠燈）
    ![img](_images/runner_status.png)

# 建立 GitLab CI/CD
## 介紹
GitLab 使用 `.gitlab-ci.yml` 管理 CI/CD 流水線的配置，此專案於 `_gitlab-ci-tpl` 資料夾提供範例樣板，請依據操作範圍選擇需要的樣板，並複製到專案的根目錄：
|  | 初始化 | 實作 |
| --- | --- | --- |
| CI/CD | [`01a_.gitlab-ci.yml`](./01a_.gitlab-ci.yml) | [`01b_.gitlab-ci.yml`](./01b_.gitlab-ci.yml)|
| Secure CI/CD | [`02a_.gitlab-ci.yml`](./02a_.gitlab-ci.yml) | [`02b_.gitlab-ci.yml`](./02b_.gitlab-ci.yml)|

e.g.,
```sh
$ cp ./_gitlab-ci-tpl/02a_.gitlab-ci.yml .gitlab-ci.yml
```

## 加強變更保護機制
- 禁止**驗證失敗**的變更
  - [專案 Menu] `Settings` > `Merge requests` > `Merge checks`
    - [x] Pipeline must succeed
  ![](_images/pipeline-must-succeed.png)
- 禁止直接變更**主幹**
  - [專案 Menu] `Settings` > `Repository` > `Protected branches`
    - `[main]` 將 **Allow to push** 設為 **No one**
  ![](_images/protect-main.png)

## 建立 CI/CD Pipeline
1. 將選用的 GitLab CI 樣板重新命名為 `.gitlab-ci.yml`，並複製到 GitLab 程式碼儲存庫的**根目錄**：
    ```sh
    # e.g.,
    $ cp ./_gitlab-ci-tpl/02a_.gitlab-ci.yml .gitlab-ci.yml
    ```
2. 提交 `.gitlab-ci.yml`
    ```sh
    $ git add .gitlab-ci.yml
    $ git commit -m "feat: implement cicd"
    $ git push
    ```
3. 查看 Pipeline 的執行狀況
   - Merge Request
     - ![img](_images/pipeline-in-mr.png)
   - [專案 Menu] `CI/CD` > `Pipelines` 可看到**該次觸發**與**全部** Pipeline 的執行狀況
     - ![img](_images/run-pipeline.png)


# 安全問題與修正
基於 `02b_.gitlab-ci.yml` 導入自動化安全測試，可以發現此範例包含了以下的安全風險：
1. **container_scanning**: [FAILED]
   - ejs 3.1.6 含有 CVE-2022-29078 (CRITICAL)
2. **secret_detection**: [FAILED]
   - configs/aws.json 與 README 中包含了敏感資訊
3. **nodejs-scan-sast** [該關卡不會顯示失敗，請透過 Job artifacts > Download 下載測試報告]:
   - 程式碼包含 node_sqli_injection

修正的方式:
https://gitlab.com/cphttraining/devsecops-20221213/d3_gitlab-cicd/-/tree/3-fix-sec-issue